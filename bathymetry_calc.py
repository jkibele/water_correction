from common import *
#from accuracy_assessment import GroundTruthShapefile

def make_bathy_array(spectra_pairs,param_vector=[-0.1706, 0.8913, -0.2316]):
    """Given 2 arrays that represent raster bands of surface reflectance,
    calculate bathymetry as shown in equation 2 of Dierssen et al. 2003."""
    depth = dierssen_function( spectra_pairs, param_vector )
    depth = np.nan_to_num( depth )
    return depth

def make_bathy_tif(inDs,spectra_pairs,param_vector=[-0.1706, 0.8913, -0.2316]):
    deptharr = make_bathy_array(spectra_pairs,param_vector)
    outfilename = inDs.GetDescription().replace('.tif','_bathy.tif')
    output_gtif_like_img(inDs, np.array([deptharr]), outfilename, no_data_value=-99)
    print "Wrote bathymetry to %s." % outfilename
    
def dierssen_function( spectra_pairs, abc_vector = [-0.1706, 0.8913, -0.2316] ):
    """
    Function for calculating bathymetry from reflectance from Dierssen et al.
    2003, equation 2.
    """
    x = np.log( spectra_pairs[0] / spectra_pairs[1] )
    a = abc_vector[0]    
    b = abc_vector[1]
    c = abc_vector[2]
    return np.exp(a*x**2 + b*x + c)
    
def merit_function( x_band_pairs, y_depth, depth_function=dierssen_function ):
    """
    Return a merit function that just takes the parameter vector as an arguement.
    """
    p_vec_func = lambda p_vec: ( ( depth_function( x_band_pairs, abc_vector=p_vec ) - y_depth )**2 ).sum()
    return p_vec_func
    
def dierssen_optimal_vector( x_band_pairs, y_depths, initial_vec=[-0.1706, 0.8913, -0.2316] ):
    """
    Return the optimal parameter vector using scipy.optimize.minimize
    """
    from scipy.optimize import minimize
    mf = merit_function( x_band_pairs, y_depths, dierssen_function )
    res = minimize( mf, initial_vec )
    return res.x
