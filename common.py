from osgeo import gdal, ogr
from osgeo.gdalconst import *
import numpy as np
import matplotlib.pyplot as plt
import scipy
import sys, os, argparse, itertools
import spectral as sp
from mpl_toolkits.mplot3d import Axes3D

class RasterDS(object):
    """
    A raster of an passive optical remote sensing scene. For the current usage
    this will be WorldView-2 or Quickbird Imagery but I will try to keep it 
    general enough to work with any multispectral or (possibly) hyperspectral
    passive optical remote sensing data.
    
    We're dealing with GeoTiffs which GDAL is happy with but spectral (spy) won't touch 
    and LANs for spectral which GDAL will sort of deal with. This RasterDS object can
    take both types, do conversions, and expose the functionality we need.
    """
    def __init__(self, file_path, overwrite=False):
        self.file_path = file_path
        self.overwrite = overwrite
        if os.path.basename(self.file_path).lower().endswith('.tif'):
            #self.driver = gdal.GetDriverByName('GTiff')
            self.file_type = 'tif'
        elif os.path.basename(self.file_path).lower().endswith('.lan'):
            #self.driver = gdal.GetDriverByName('LAN')
            self.file_type = 'lan'
        elif os.path.basename(self.file_path).lower().endswith('.gis'):
            #self.driver = gdal.GetDriverByName('LAN')
            self.file_type = 'gis'
        else:
            raise ValueError("I do not know how to deal with this file type yet. Sorry.")
        self.gdal_ds = open_raster(self.file_path)
        
    @property
    def output_file_path(self):
        """
        Return a file path for output. Assume that we'll output same file extension.
        """
        if self.overwrite:
            return self.file_path
        else:
            f_ext = self.file_type
            fname = self.file_path
            add_num = 0
            while os.path.exists(fname):
                add_num += 1
                if add_num==1:
                    fname = fname.replace( os.path.extsep + f_ext, '_%i' % add_num + os.path.extsep + f_ext )
                else:
                    old = '_%i.%s' % ( add_num - 1, f_ext )
                    new = '_%i.%s' % ( add_num, f_ext )
                    fname = fname.replace( old, new )
            return fname
        
    @property
    def band_array(self):
        return bandarr_from_ds(self.gdal_ds)
        
    @property
    def band_array_int16(self):
        return self.band_array.astype(np.int16)
        
    @property
    def spy_image(self):
        """
        Return a spectral python (spy) image object. The can't be created directly from
        a tif so there may be some conversion needed.
        """
        if self.file_type=='lan':
            return sp.image(self.file_path)
        else:
            lan_fp = os.path.extsep.join([self.output_file_path.rsplit(os.path.extsep)[0],'lan'])
            if os.path.exists( lan_fp ):
                return sp.image( lan_fp )
            else:
                return self.output_LAN()
        
    def output_LAN(self,outfilepath=None):
        no_data_value = -99
        if outfilepath:
            outfp = outfilepath
        else:
            outfp = os.path.extsep.join([self.output_file_path.rsplit(os.path.extsep)[0],'lan'])
        lan_dr = gdal.GetDriverByName('LAN')
        cols = self.gdal_ds.RasterXSize
        rows = self.gdal_ds.RasterYSize
        out_ds = lan_dr.Create( outfp, cols, rows, self.gdal_ds.RasterCount, gdal.GDT_Int16 )
        # georeference the image and set the projection
        out_ds.SetGeoTransform(self.gdal_ds.GetGeoTransform())
        out_ds.SetProjection(self.gdal_ds.GetProjection())
        
        # write the data
        bandarr = self.band_array_int16
        for bandnum in range(1,len(bandarr) + 1):  # bandarr is zero based index while GetRasterBand is 1 based index
            outBand = out_ds.GetRasterBand(bandnum)
            outBand.WriteArray(bandarr[bandnum - 1])
            outBand.FlushCache()
            outBand.SetNoDataValue(no_data_value)
        
        # build pyramids
        gdal.SetConfigOption('HFA_USE_RRD', 'YES')
        out_ds.BuildOverviews(overviewlist=[2,4,8,16,32,64,128])
        
        return sp.image( outfp )
        
    def cluster(self, nclusters, outfp=None):
        (m,c) = sp.cluster( self.spy_image.load() )
        if not outfp:
            outfp = os.path.dirname(self.output_file_path) + os.path.sep + os.path.basename(self.output_file_path).split(os.path.extsep)[0] + '_clu' + str(nclusters) + os.path.extsep + 'tif'
        output_gtif_like_img( self.gdal_ds, np.array([m]), outfp, dtype=GDT_Int16 )
        return( RasterDS( outfp ) )
        
    def kmeans(self, nclusters, tries, outfp=None):
        (m,c) = sp.kmeans(self.spy_image.load(), nclusters, tries)
        if not outfp:
            outfp = os.path.dirname(self.output_file_path) + os.path.sep + os.path.basename(self.output_file_path).split(os.path.extsep)[0] + '_kmeans' + str(nclusters) + os.path.extsep + 'tif'
        output_gtif_like_img( self.gdal_ds, np.array([m]), outfp, dtype=GDT_Int16 )
        return( RasterDS( outfp ) )
        
    def gaussian_mlc(self, gts, buffer_radius=None):
        classes = gts.training_classes(self,buffer_radius=buffer_radius)
        gmlc = sp.GaussianClassifier(classes)
        clmap = gmlc.classify_image(self.spy_image.load())
        out_fp = self.output_file_path.rsplit( os.path.extsep, 1 )[0] + os.path.extsep + 'tif'
        # band arrays can have multiple bands so I need to make a single 
        # band array for clmap
        clmap = np.array([ clmap ])
        output_gtif_like_img(self.gdal_ds,clmap,out_fp,dtype=gdal.GDT_Int16)
        return RasterDS( out_fp )
        
    def index_at_point(self,point):
        """
        Return the matrix index for a point.
        """
        x = point.GetX()
        y = point.GetY()

        trans = transform_dict( self.gdal_ds )
        
        xOffset = int( (x - trans['originX']) / trans['pixWidth'] )
        yOffset = int( (y - trans['originY']) / trans['pixHeight'] )
        
        return yOffset, xOffset
        
    def spectrum_at_point(self,point):
        """
        Return the spectra at a given point. The spectra will be returned as a numpy
        array, 1 row x N columns where N is the number of bands in the image. This 
        assumes that self is a multispectral image and not a ground truth raster. I 
        should probably figure out how to limit this method to the RasterDS class. I
        can't imagine wanting to use it on a GroundTruthRaster.
        """
        yOffset, xOffset = self.index_at_point( point )
        
        band_values = []
        for bnum in range( 1, self.gdal_ds.RasterCount + 1 ):
            band = self.gdal_ds.GetRasterBand( bnum )
            data = band.ReadAsArray(xOffset, yOffset, 1, 1)[0,0]
            band_values.append( data )
        return np.array( band_values )
    
    def habitat_spectra(self, gts):
        """
        Return a dictionary of spectra for each habitat in gts. 
        """
        hsd = {}
        if gts.geometry_type=='point':
            for hab in gts.habitats:
                spectra_list = []
                for feat in gts.hab_dict[hab]:
                    spectrum = self.spectrum_at_point( feat.geometry() )
                    spectra_list.append( spectrum )
                hsd[hab] = np.array( spectra_list )
        elif gts.geometry_type=='polygon':
            # get a ground truth raster array
            gtr = gts.rasterize( raster_template=self, array_only=True )
            # get a band array and rearrange it so that when I give a pixel index,
            # I get a one dimensional array that shows values across bands
            barr = np.swapaxes( self.band_array.T, 0, 1 )
            for hab, hab_code in gts.habitat_codes.items():
                hab_indices = np.where( gtr==hab_code )
                hab_spect_array = barr[ hab_indices ]
                hsd[hab] = hab_spect_array
        elif gts.geometry_type=='line':
            # find out how wide pixels are
            pixelwidth = self.gdal_ds.GetGeoTransform()[1]
            # buffer the lines into polygons by pixelwidth
            poly_gts = gts.buffer(radius=pixelwidth)
            # get hab spectra dictionary for polys
            hsd = poly_gts.habitat_spectra
        return hsd
        
    def plot_habitat_spectra(self, gts, print_spectra=False):
        """
        Generate a plot of the spectra for the points in gts. Currently assuming that gts
        is points. Will need to change it to use polygons.
        """
        fig = plt.figure()
        hsd = self.habitat_spectra(gts)
        band_count = self.gdal_ds.RasterCount
        for hab, spectra in hsd.items():
            print hab
            ax = fig.add_subplot(111)
            for spectrum in spectra:
                if print_spectra:
                    print spectrum
                band_nums = np.array( range( 1, band_count + 1 ) )
                if hab:
                    ax.plot( band_nums, spectrum, marker='.',linestyle='-', color=gts.hab_colors[hab] )
        plt.show()
        
    def spectra_in_band_space(self, gts, bandlist):
        hsd = self.habitat_spectra( gts )
        bs_dict = {}
        for hab, spectra in hsd.items():
            band_dict = {}
            for band_count in range( len(bandlist) ):
                band = bandlist[band_count]
                splist = []
                for spectrum in spectra:
                    splist.append( spectrum[ band - 1 ] ) # band number 1 is index 0 etc.
                band_dict[band] = splist
            bs_dict[hab] = band_dict
        return bs_dict
        
    def plot_spectra_in_band_space(self, gts, bandlist):
        bs_dict = self.spectra_in_band_space(gts, bandlist)
        fig = plt.figure()
        plt.xlabel('Band ' + str( bandlist[0]) )
        plt.ylabel('Band ' + str( bandlist[1]) )
        if len(bandlist) < 3:
            ax = fig.add_subplot(111)
        else:
            ax = fig.add_subplot(111, projection='3d')
            #plt.zlabel('Band ' + str(bandlist[2]) ) # I don't know how to label the z axis
        for hab, band_dict in bs_dict.items():
            if not hab:
                continue
            bd_keys = band_dict.keys()[:3] # I can only plot in three dimensions or two
            if len(bd_keys) < 3:
                ax.scatter(band_dict[bd_keys[0]],band_dict[bd_keys[1]], color=gts.hab_colors[hab])
            else:
                ax.scatter(band_dict[bd_keys[0]],band_dict[bd_keys[1]],band_dict[bd_keys[2]], color=gts.hab_colors[hab])
        plt.show()
    
    def habitat_depth_band_data( self, gts, band, hab ):
        feats = gts.hab_dict[hab]
        x = []
        y = []
        for feat in feats:
            if feat.depth:
                y.append( feat.depth )
                x.append( self.spectrum_at_point( feat.geometry() )[band - 1] )
        return x,y
        
    def depth_band_data( self, gts, band ):
        feats = gts.features
        x = []
        y = []
        for feat in feats:
            if feat.depth:
                y.append( feat.depth )
                x.append( self.spectrum_at_point( feat.geometry() )[band - 1] )
        return x,y
        
    def depth_band_plot( self, gts, band ):
        """
        For each hab type in gts, get spectra for each point and depth for each
        point. Plot the depths against the pixel values.
        """
        fig = plt.figure()
        plt.xlabel('Pixel Value in Band ' + str( band ) )
        plt.ylabel('Depth (m)')
        ax = fig.add_subplot(111)
        for hab in gts.legit_habs:
            x,y = self.habitat_depth_band_data(gts,band,hab)
            ax.scatter(x,y, color=gts.hab_colors[hab])
        plt.show()
        
    def depth_band_ratio_plot( self, gts, band1, band2 ):
        """
        For each hab type in gts, get spectra for each point and depth for each
        point. Plot the depths against the pixel values.
        """
        fig = plt.figure()
        plt.xlabel('Pixel Value in Band Ratio %i / %i ' % ( band1 , band2 ) )
        plt.ylabel('Depth (m)')
        ax = fig.add_subplot(111)
        for hab in gts.legit_habs:
            feats = gts.hab_dict[hab]
            x = []
            y = []
            for feat in feats:
                if feat.depth:
                    y.append( feat.depth )
                    pspec = self.spectrum_at_point( feat.geometry() )
                    # Calculate ratio
                    x.append( pspec[band1 - 1] / float(pspec[band2 - 1]) )
                    #print pspec
            #print x
            #print y
            ax.scatter(x,y, color=gts.hab_colors[hab])
        plt.show()
        
    def bathy_array(self, short_band_num, long_band_num, param_vector=[-0.1706, 0.8913, -0.2316]):
        """
        Generate bathymetry from multispectral imagery using equation 2 from 
        Dierssen et al. 2003. Return the array of values.
        """
        from bathymetry_calc import make_bathy_array
        # subtract 1 so that band 1 returns the first band
        short_band = self.band_array[short_band_num - 1]
        long_band = self.band_array[long_band_num - 1]
        specta_pairs = np.array( [ short_band, long_band ] )
        bathy_arr = make_bathy_array( specta_pairs, param_vector )
        return bathy_arr
        
    def optimized_bathy_array(self, gts, short_band_num, long_band_num):
        dov = self.dierrsen_optimal_params(gts,short_band_num,long_band_num)
        return self.bathy_array(short_band_num,long_band_num,dov)
        
    def bathy_image(self, short_band_num, long_band_num, overwrite=False, filename=None):
        """
        Generate bathymetry from multispectral imagery using equation 2 from 
        Dierssen et al. 2003. Write to a geotiff file.
        """
        # figure out filename for output
        if not filename:        
            f_ext = os.path.extsep + self.file_type
            filename = self.file_path.replace( f_ext, "_bathy" + f_ext )
        if os.path.exists(filename) and not overwrite:
            new_rds = RasterDS( filename )
            filename = new_rds.output_file_path
        output_gtif_like_img(self.gdal_ds, np.array([self.bathy_array(short_band_num,long_band_num)]), filename, no_data_value=-99 )
        return RasterDS( filename )
        
    def optimized_bathy_image(self, gts, short_band_num, long_band_num, filename=None, overwrite=False):
        """
        Generate bathymetry from multispectral imagery using equation 2 from 
        Dierssen et al. 2003. Write to a geotiff file.
        """
        if not filename:        
            f_ext = os.path.extsep + self.file_type
            filename = self.file_path.replace( f_ext, "_bathy" + f_ext )
        if os.path.exists(filename) and not overwrite:
            new_rds = RasterDS( filename )
            filename = new_rds.output_file_path
        oba = self.optimized_bathy_array(gts,short_band_num,long_band_num)
        output_gtif_like_img(self.gdal_ds, np.array([oba]), filename, no_data_value=-99 )
        return RasterDS( filename )
        
    def bathy_optimization_data(self, gts, short_band_num, long_band_num):
        """
        Given a GroundTruthShapefile and two band numbers return two arrays.
        The first array will be the ratio values for Dierssen et al. 2003
        equation 2. The second array will be the depths at those points from 
        the GroundTruthShapefile.
        """
        # get all points that have a depth value
        points = [f.geometry() for f in gts.features if f.depth]
        # get array of depths for those points
        depths = np.array( [f.depth for f in gts.features if f.depth] )
        # get the spectra for each point and slice it so we keep only two bands
        spectra = np.array( [self.spectrum_at_point(p) for p in points] )[:,[short_band_num - 1,long_band_num - 1]]
        # find indicies of non-zero values to avoid division by zero and trying
        # to calculate log of zero
        nonz = np.concatenate( (np.where( spectra[:,0] ), np.where( spectra[:,1] )),1 )
        nonz = np.unique(nonz)
        # get rid of depths that correspond to the zeros
        depths = depths[nonz]
        # arrange into a 2 x N array
        specta_pairs = np.vstack( ( spectra[nonz,0], spectra[nonz,1] ) )
        # return valid ratios and depths
        assert( specta_pairs.shape[1]==depths.shape[0] )
        return specta_pairs, depths
        
    def dierrsen_optimal_params(self, gts, short_band_num, long_band_num):
        from bathymetry_calc import dierssen_optimal_vector
        pairs, depths = self.bathy_optimization_data( gts, short_band_num, long_band_num )
        return dierssen_optimal_vector( pairs, depths, initial_vec=[-0.1706, 0.8913, -0.2316] )
        
    def new_image_from_array(self,bandarr,outfilename=None):
        if not outfilename:
            outfilename = self.output_file_path
        output_gtif_like_img(self.gdal_ds, bandarr, outfilename, no_data_value=-99, dtype=GDT_Float32)
        return RasterDS(outfilename)
        
    def ward_cluster_land_mask(self,threshold=50):
        """
        Try to seperate land from water using scikits-learn ward clustering. The 
        simple land_to_zeros method above does not distinguish shadow pixels on land
        from water pixels. The Ward clustering conectivity constraint should take
        care of that.
        """
        from sklearn.cluster import Ward    
        from sklearn.feature_extraction.image import grid_to_graph
        import time
        # Get the last band. I'm assuming the last band will be the longest
        # wavelength.
        band = self.band_array[-1]
        # zero out pixels that are above the threshold
        band[np.where(band > threshold)] = 0
        
        X = np.reshape(band, (-1,1))
        connectivity = grid_to_graph(*band.shape)
        
        st = time.time()
        n_clusters = 2
        ward = Ward(n_clusters=n_clusters, connectivity=connectivity).fit(X)
        label = np.reshape(ward.labels_, band.shape)
        print "Elaspsed time: ", time.time() - st
        return label

class GroundTruthRaster(RasterDS):
    """
    A raster representation of a GroundTruthShapefile. Used as a training raster to create
    classes for Gaussian Classifier in spectral. Including a GroundTruthShapefile as gt_shp
    will give the raster access to hab info.
    """
    def __init__(self, file_path, gt_shp=None):
        super(GroundTruthRaster, self).__init__(file_path)
        if gt_shp:
            self.habitat_codes = gt_shp.habitat_codes
            self.habitat_codes['Unclassified'] = 0
            self.habitat_names = dict( zip( self.habitat_codes.values(), self.habitat_codes.keys() ) )
        else:
            self.habitat_codes = None
            self.habitat_names = None
        
    @property
    def histogram_dict(self):
        arr = self.band_array[0]
        hab_codes = np.unique(arr).tolist()
        hab_codes.append( 1 + arr.max() )
        hist = np.histogram( arr, bins=hab_codes )
        return dict( zip( hist[1], hist[0] ) )
        
    @property
    def histogram_with_names(self):
        d = {}
        for k,v in self.histogram_dict.items():
            d[ self.habitat_names[k] ] = v
        return d
        
    def create_error_matrix(self, class_raster):
        """
        Create an error matrix using self as reference map and class_raster as the 
        comparison map.
        """
        # for now, only work with rasters of the same size and shape
        ref_mat = self.band_array
        if len( ref_mat.shape )==3:
            ref_mat = ref_mat[0]
        comp_mat = class_raster.band_array
        if len( comp_mat.shape )==3:
            comp_mat = comp_mat[0]
        class_set = list( set( ref_mat.flatten() ) )
        
class ClassificationRaster( GroundTruthRaster ):
    """
    A class for classifcation map images.
    """
    def __init__(self, file_path, gt_shp=None):
        super(ClassificationRaster, self).__init__(file_path,gt_shp=gt_shp)
        
    def value_at_point(self,point):
        return self.spectrum_at_point(point)[0]
        

def open_raster(filename):
    """Take a file path as a string and return a gdal datasource object"""
    # register all of the GDAL drivers
    gdal.AllRegister()

    # open the image
    img = gdal.Open(filename, GA_ReadOnly)
    if img is None:
        print 'Could not open %s' % filename
        sys.exit(1)
    else:
        return img
        
def get_band_combos(img):
    """Given an image datasource, find out how many bands there are and
    return all possible combinations."""
    bands = img.RasterCount
    return [x for x in itertools.combinations(range(1,bands+1),2)]
        
def transform_dict(img):
    """Take a raster data source and return a dictionary with geotranform
    values and keys that make sense"""
    geotrans = img.GetGeoTransform()
    ret_dict = {
            'originX':   geotrans[0],
            'pixWidth':  geotrans[1],
            'rotation1': geotrans[2],
            'originY':   geotrans[3],
            'rotation2': geotrans[4],
            'pixHeight': geotrans[5],
        }
    return ret_dict
    
def open_shapefile(filename):
    """Take a file path string and return an ogr shape"""
    # open the shapefile and get the layer
    driver = ogr.GetDriverByName('ESRI Shapefile')
    shp = driver.Open(filename)
    if shp is None:
        print 'Could not open %s' % filename
        sys.exit(1)
    return shp
    
def envelope_dict(geom):
    """take a geometry and return a dictionary of envelope coordinates
    with descriptive keys"""
    # GetEnvelope returns (xLeft,xRight,yBottom,yTop) in map coords
    return dict(zip(('xLeft','xRight','yBottom','yTop'),geom.GetEnvelope()))
    
def pixels_in_envelope(shp,img):
    """Compare area of all pixels within envelope to area of envelope.
    This is just to check that the number of pixels I'm reporting 
    elsewhere is accurate."""
    band = img.GetRasterBand(1) # I only want the pixel dimensions, not values so which band I use is arbitrary. I'll use 1
    lyr = shp.GetLayer()
    lyr.ResetReading()
    trans = transform_dict(img)
    for feat in lyr:
        geom = feat.geometry()
        pixarr = pixel_array_from_geom(geom,band,trans,print_out=True)
        
def output_confirmation_raster(shp,img,band):
    """Output a raster containing the pixels used for calculations to 
    verify that they line up with the geometries."""
    lyr = shp.GetLayer()
    lyr.ResetReading()
    trans = transform_dict(img)
    driver = img.GetDriver()
    rows = img.RasterYSize
    cols = img.RasterXSize
    outDs = driver.Create('confirmation.tif', cols, rows, 1, GDT_Int16)
    if outDs is None:
        print 'Could not create confirmation.tif'
        sys.exit(1)

    outBand = outDs.GetRasterBand(1)
    
    for feat in lyr:
        geom = feat.geometry()
        pixarr,xOffset,yOffset = pixel_array_from_geom(geom,band,trans,False,True)
        outBand.WriteArray(pixarr,xOffset,yOffset)
    outBand.FlushCache()
    outBand.SetNoDataValue(-99)
    # georeference the image and set the projection
    outDs.SetGeoTransform(img.GetGeoTransform())
    outDs.SetProjection(img.GetProjection())

    # build pyramids
    gdal.SetConfigOption('HFA_USE_RRD', 'YES')
    outDs.BuildOverviews(overviewlist=[2,4,8,16,32,64,128])

def zeros_to_nans(arr):
    """Given a numpy array, replace all zeros and negatives with nan. If arr is not
    floats, try to make it floats."""
    if arr.dtype.kind != 'f':
        arr = np.float_(arr)
    arr[np.where(arr<=0.0)] = np.nan
    return arr
    
def mean_minus_2_stdev(arr):
    """Given a numpy array, return it's mean minus 2 standard deviations. This 
    will be used in dark pixel calculation."""
    return arr.mean() - 2 * arr.std()
    
def land_to_zeros(img,threshold=50):
    """
    Take a dataset, find out which pixels in the highest numbered band have a 
    value above threshold, then set all those pixels to zero in the other 
    bands.
    
    :param img: A gdal datasource
    :type img: osgeo.gdal.Dataset
    
    :param threshold: Pixel value above which is assumed to be land.
    :type threshold: int or float
    """
    # get the highest numbered band - I'm assuming this is the longest wavelength NIR band
    nirband = img.GetRasterBand(img.RasterCount)
    nirarr = nirband.ReadAsArray()
    for bandnum in range(1,img.RasterCount + 1): # leave the nirband alone
        band = img.GetRasterBand(bandnum)
        barr = band.ReadAsArray()
        barr[np.where(nirarr > threshold)] = 0
        if bandnum==1:
            bandarr = np.array([barr])
        else:
            bandarr = np.append(bandarr,[barr],axis=0)
        # print "bandarr shape: %s" % str(bandarr.shape)
    output_gtif_like_img(img,bandarr,img.GetDescription().replace('.tif','_landzero.tif'))
    
def bandarr_from_ds(img):
    """Take a raster datasource and return a band array. Each band is read
    as an array and becomes one element of the band array."""
    for band in range(1,img.RasterCount + 1):
        barr = img.GetRasterBand(band).ReadAsArray()
        if band==1:
            bandarr = np.array([barr])
        else:
            bandarr = np.append(bandarr,[barr],axis=0)
    return bandarr

def output_gtif(bandarr, cols, rows, outfilename, geotransform, projection, no_data_value=-99, driver_name='GTiff', dtype=GDT_Float32):
    """Create a geotiff with gdal that will contain all the bands represented
    by arrays within bandarr which is itself array of arrays."""
    # make sure bandarr is a proper band array
    if len( bandarr.shape )==2:
        bandarr = np.array([ bandarr ])
    driver = gdal.GetDriverByName(driver_name)
    outDs = driver.Create(outfilename, cols, rows, len(bandarr), dtype)
    if outDs is None:
        print "Could not create %s" % outfilename
        sys.exit(1)
    for bandnum in range(1,len(bandarr) + 1):  # bandarr is zero based index while GetRasterBand is 1 based index
        outBand = outDs.GetRasterBand(bandnum)
        outBand.WriteArray(bandarr[bandnum - 1])
        outBand.FlushCache()
        outBand.SetNoDataValue(no_data_value)
        
    # georeference the image and set the projection
    outDs.SetGeoTransform(geotransform)
    outDs.SetProjection(projection)

    # build pyramids
    gdal.SetConfigOption('HFA_USE_RRD', 'YES')
    outDs.BuildOverviews(overviewlist=[2,4,8,16,32,64,128])
    
def output_gtif_like_img(img, bandarr, outfilename, no_data_value=-99, dtype=GDT_Float32):
    """Create a geotiff with attributes like the one passed in but make the
    values and number of bands as in bandarr."""
    cols = img.RasterXSize
    rows = img.RasterYSize
    geotransform = img.GetGeoTransform()
    projection = img.GetProjection()
    output_gtif(bandarr, cols, rows, outfilename, geotransform, projection, no_data_value, driver_name='GTiff', dtype=dtype)
        
def ditch_zeros(xarray,yarray):
    """Take two arrays and drop all the zeros out of them assuming that
    they are matched x,y values. So if I drop a zero from x, I drop the 
    corresponding value from y, whether it's a zero or not and vice
    versa."""
    xarray,yarray = xarray[xarray.nonzero()],yarray[xarray.nonzero()]
    xarray,yarray = xarray[yarray.nonzero()],yarray[yarray.nonzero()]
    return xarray,yarray

def pixel_array_from_geom(geom,band,trans,print_out=False,return_offsets=False):
    """Take a geom, find its envelope (bounding box, basically), pull
    the pixels from band, and pass them back as an array. Have to 
    provide pixel width and height and origins so pass in a 
    transform dictionary from the transform_dict method too."""
    elp = envelope_dict(geom)
    xtop = elp['xLeft']
    ytop = elp['yTop']
    xOffset = int( (xtop - trans['originX']) / trans['pixWidth'] )
    yOffset = int( (ytop - trans['originY']) / trans['pixHeight'] )
    xdist = elp['xRight'] - elp['xLeft']
    ydist = elp['yBottom'] - elp['yTop']
    cols = int( xdist / trans['pixWidth'] )
    rows = int( ydist / trans['pixHeight'] )
    if print_out:
        pix_area = cols * trans['pixWidth'] * rows * trans['pixHeight']
        env_area = xdist * ydist
        print "xOffset: %i, yOffset: %i, cols: %i, rows: %i, Pixel Area: %.2f, Envelope Area: %.2f" % (xOffset,yOffset,cols,rows,pix_area,env_area)
    if return_offsets:
        return band.ReadAsArray(xOffset, yOffset, cols, rows),xOffset,yOffset
    else:
        return band.ReadAsArray(xOffset, yOffset, cols, rows)

def get_x_and_y_pixel_arrays(geom,img,xband,yband):
    """Take a geometry, an image datasource, and two integers and return two
    arrays. The arrays will have the pixel values for their respective bands
    that fall within the envelope of geom. Note: that's not within the geom,
    it is within the envelope of the geom. If you have a square, that will 
    be the same thing. If not, you might end up confused. I am confused and 
    I wrote it."""
    trans = transform_dict(img)
    x = pixel_array_from_geom(geom,img.GetRasterBand(xband),trans)
    y = pixel_array_from_geom(geom,img.GetRasterBand(yband),trans)
    return x, y
    
def get_arrays_for_all_geoms(shp,img,xband,yband):
    """Take a shapefile datasource, an image datasource, and two integers and
    return two arrays. The arrays will contain pixel values taken from img 
    from all pixels that are within the bounding envelope (bounding box) of
    each geometry in shp."""
    lyr = shp.GetLayer()
    xout = []
    yout = []
    lyr.ResetReading()
    
    for feat in lyr: # I am assuming one geometry per feature. May not be safe if there are multipart geometries
        x, y = get_x_and_y_pixel_arrays(feat.geometry(),img,xband,yband)
        xout = np.append(xout,x)
        yout = np.append(yout,y)
        
    return xout, yout

def how_many_each_pair(xarr,yarr):
    """Given two arrays, return a dictionary that has the pair as a key
    and the number of occurances as a value for each unique pair."""
    parr = [tuple(i) for i in zip(xarr,yarr)]
    unique_pairs = np.unique(parr)
    ufreq = [parr.count(tuple(i)) for i in unique_pairs]
    return dict(zip([tuple(i) for i in unique_pairs],ufreq))
    
def freq_list(xarr,yarr):
    """Given two arrays, return a list of frequency values that matches
    the frequency of each pair of values."""
    udict = how_many_each_pair(xarr,yarr)
    return [udict[k] for k in tuple(zip(xarr,yarr))]

def pick_a_pixel(rfile,sfile):
    """
    Just pick the first geom from the first feature in a shapefile, 
    get its centroid, and return some info about the corresponding pixel
    in the raster. This is just for testing it's not really useful.
    """
    shp = open_shapefile(sfile)
    img = open_raster(rfile)
    
    lyr = shp.GetLayer()
    feat = lyr.GetNextFeature()
    geom = feat.geometry()
    cent = geom.Centroid()
    x = cent.GetX()
    y = cent.GetY()
    
    trans = transform_dict(img)
    band = img.GetRasterBand(1) # just get one band for now
    xOffset = int( (x - trans['originX']) / trans['pixWidth'] )
    yOffset = int( (y - trans['originY']) / trans['pixHeight'] )
    
    data = band.ReadAsArray(xOffset, yOffset, 1, 1)
    return data[0,0]

def power_fit(xdata,ydata):
    # Power-law fitting is best done by first converting
   # to a linear equation and then fitting to a straight line.
   #
   #  y = a * x^b
   #  log(y) = log(a) + b*log(x)
   #
   
   logx = np.log10(xdata)
   logy = np.log10(ydata)
   logyerr = yerr / ydata
   
   # define our (line) fitting function
   fitfunc = lambda p, x: p[0] + p[1] * x
   errfunc = lambda p, x, y, err: (y - fitfunc(p, x)) / err
   
   pinit = [1.0, -1.0]
   out = scipy.optimize.leastsq(errfunc, pinit,
                          args=(logx, logy, logyerr), full_output=1)
   
   pfinal = out[0]
   covar = out[1]
   print pfinal
   print covar
   
   index = pfinal[1]
   amp = 10.0**pfinal[0]
   
   indexErr = sqrt( covar[0][0] )
   ampErr = sqrt( covar[1][1] ) * amp
   return out



