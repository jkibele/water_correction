from common import *
from scipy import stats
from Tkinter import Tk
import pylab
import scipy

test_file = 'test_data/tawf8band_nztm_landzero.tif'
test_regions = 'test_data/test_lines.shp'
deep_shp = 'test_data/optically_deep.shp'

def simple_tests(from_commandline):
    """Take a namespace object from the parser and do some stuff with it for testing purposes"""
    img = open_raster(from_commandline.raster)
    print "The image is %i by %i pixels and has %i bands\n" % (img.RasterXSize, img.RasterYSize, img.RasterCount)
    shape_layer = open_shapefile(from_commandline.shapefile)
    pix_value = pick_a_pixel(from_commandline.raster,from_commandline.shapefile)
    print "The value of that pixel in band 1 is %s" % str(pix_value)
    extract_regress_plot(shape_layer,img,from_commandline.xband,from_commandline.yband,True)
    
def setup_test_variables():
    """I keep finding myself repeating multiple steps to do interactive
    testing. I hate repeating multiple steps."""
    shp = open_shapefile(test_regions)
    lyr = shp.GetLayer()
    feat = lyr.GetNextFeature()
    geom = feat.geometry()
    img = open_raster(test_file)
    trans = transform_dict(img)
    band = img.GetRasterBand(1) # just get one band for now
    return shp,lyr,feat,geom,img,trans,band

def dark_pixel_calculation(shp,img,bandnum):
    """Take the features from a shapefile, find the pixels in img that are
    covered by the envelope of those features, and return their average value for a band
    minus 2 standard deviations. To be used as in Equation 7.1 of Lesson 7: Compensating
    for variable water depth from UNESCO's BILKO documentation. That in turn
    was taken from Lyzenga, 1978 Passive remote sensing techniques for mapping
    water depth and bottom features. That was actually taken from Polcyn 1970, I think.
    I need to straighten all that out and organize and document this whole thing
    better.
    
    I'm not really using this method now. I'm using the dark_pixel_subtraction method
    instead. It is similar but instead of requiring a shapefile input to identify dark
    pixels, it uses the dark_pixel_finder method to locate them."""
    lyr = shp.GetLayer()
    lyr.ResetReading()
    parr = np.array([])
    for feat in lyr:
        geom = feat.geometry()
        trans = transform_dict(img)
        band = img.GetRasterBand(bandnum)
        parr = np.append(parr, pixel_array_from_geom(geom,band,trans,print_out=False,return_offsets=False))
    print "dp value for band %i and shape %s is: %.3f" % (bandnum, shp.name, mean_minus_2_stdev(parr))
    return mean_minus_2_stdev(parr)
    
def depth_invariant_band(shp,img,outDs,outBand,xband,yband):
    """Output a depth invariant band to be put int and output raster."""
    lyr = shp.GetLayer()
    lyr.ResetReading()
    trans = transform_dict(img)
    driver = img.GetDriver()
    rows = img.RasterYSize
    cols = img.RasterXSize
    
def depth_invariant_raster(shp,img,bandpairs,dark_pixel_shapefile=False,outpath=None):
    """Given an array of band pairs, output a raster with as many bands
    as there are band pairs. Each band of the output will be a depth 
    invariant index derived from the bands in the band pair.
    
    bandpairs of the form: array([[3, 2],
                                [4, 2],
                                [4, 3]])"""
    lyr = shp.GetLayer()
    lyr.ResetReading()
    trans = transform_dict(img)
    driver = gdal.GetDriverByName('GTiff')
    rows = img.RasterYSize
    cols = img.RasterXSize
    if outpath:
        outname = outpath
    else:
        outname = 'depth_invariant_output.tif' 
    outDs = driver.Create(outname, cols, rows, len(bandpairs), GDT_Float32)
    if outDs is None:
        print 'Could not create depth_invariant_output.tif'
        sys.exit(1)
    band_count = 1
    for band in bandpairs:
        xband = int(band[0])
        yband = int(band[1])
        outBand = outDs.GetRasterBand(band_count)
        xarr = img.GetRasterBand(xband).ReadAsArray()
        yarr = img.GetRasterBand(yband).ReadAsArray()
        if dark_pixel_shapefile:
            dp_shp = open_shapefile(dark_pixel_shapefile)
            xarr = xarr - dark_pixel_calculation(dp_shp,img,xband)
            yarr = yarr - dark_pixel_calculation(dp_shp,img,yband)
            """Okay, this is weird. I did this calculation wrong first and the results looked better
            than doing it correctly. instead of what's above here, I had:
            xarr = xarr - mean_minus_2_stdev(xarr)
            yarr = yarr - mean_minus_2_stdev(yarr)
            I think this resulted in somewhat smaller values being subtracted because the standard 
            deviations were so much larger because the whole raster was being used. I need to look
            into this more."""
        xarr = np.log(zeros_to_nans(xarr))
        yarr = np.log(zeros_to_nans(yarr))
        slope, intercept, r_value, p_value, std_err = extract_and_regress(shp,img,xband,yband)
        di_index = xarr - slope * yarr
        outBand.WriteArray(di_index,0,0)
        outBand.FlushCache()
        outBand.SetNoDataValue(-99)
        band_count += 1
    # georeference the image and set the projection
    outDs.SetGeoTransform(img.GetGeoTransform())
    outDs.SetProjection(img.GetProjection())

    # build pyramids
    gdal.SetConfigOption('HFA_USE_RRD', 'YES')
    outDs.BuildOverviews(overviewlist=[2,4,8,16,32,64,128])
    
def depth_invariant_output(shp,img,xband,yband):
    """Output a depth invariant index layer based on the two bands"""
    lyr = shp.GetLayer()
    lyr.ResetReading()
    trans = transform_dict(img)
    driver = img.GetDriver()
    rows = img.RasterYSize
    cols = img.RasterXSize
    outname = 'depth_invariant_%i_%i.tif' % (xband,yband)
    outDs = driver.Create(outname, cols, rows, 1, GDT_Float32)
    if outDs is None:
        print 'Could not create confirmation.tif'
        sys.exit(1)
    outBand = outDs.GetRasterBand(1)
    xarr = img.GetRasterBand(xband).ReadAsArray()
    yarr = img.GetRasterBand(yband).ReadAsArray()
    xarr = np.log(zeros_to_nans(xarr))
    yarr = np.log(zeros_to_nans(yarr))
    slope, intercept, r_value, p_value, std_err = extract_and_regress(shp,img,xband,yband)
    di_index = xarr - slope * yarr
    outBand.WriteArray(di_index,0,0)
    outBand.FlushCache()
    outBand.SetNoDataValue(-99)
    # georeference the image and set the projection
    outDs.SetGeoTransform(img.GetGeoTransform())
    outDs.SetProjection(img.GetProjection())

    # build pyramids
    gdal.SetConfigOption('HFA_USE_RRD', 'YES')
    outDs.BuildOverviews(overviewlist=[2,4,8,16,32,64,128])
    
def prep_for_plot(xarr,yarr):
    """Take raw pixel values, drop the zero value pairs, and return the
    natural logs of the remaining value pairs."""
    x,y = ditch_zeros(xarr,yarr)
    return np.log(x),np.log(y)

def linear_regress_array_logs(xarr,yarr):
    """Take two flat arrays, ditch the zeros out of them, and regress 
    their logs. Pass back all the values from scipy.stats.linregress()"""
    xarr,yarr = prep_for_plot(xarr,yarr)
    
    slope, intercept, r_value, p_value, std_err = stats.linregress(xarr,yarr)
    return slope, intercept, r_value, p_value, std_err

def plot_regression_basic(xarr,yarr,slope,intercept,r_value,p_value,std_err):
    """Take two arrays and the results for their regression from 
    scipy.stats.linregress() and set up and display a plot using pylab."""
    xarr,yarr = prep_for_plot(xarr,yarr)
    pylab.scatter(x,y)
    x_values = scipy.linspace(x.min(),x.max(),4)
    y_values = [slope * xx + intercept for xx in x_values]
    print x_values
    print y_values
    pylab.plot(x_values, y_values)
    pylab.show()

def extract_and_regress(shp,img,xband,yband,plot=False):
    """Given a shapefile, a raster, and two bands (as integers) return
    regression values from scipy.stats.linregress() and, optionally,
    plot the regression results using pylab. This method should generally
    be used when you don't really want the plot. It's a basic ugly plot
    and is just there for visual reference. Use extract_regress_plot() if 
    you want a better plot."""
    x,y = get_arrays_for_all_geoms(shp,img,xband,yband)
    slope, intercept, r_value, p_value, std_err = linear_regress_array_logs(x,y)
    if plot:
        plot_regression_basic(x,y,slope, intercept, r_value, p_value, std_err)
    
    return slope, intercept, r_value, p_value, std_err
    
def all_combo_print(shp,img):
    """Get regression info for all combos and print to the screen."""
    print "xband, yband, slope, intercept, r_value, std_err"
    for combo in get_band_combos(img):
        slope, intercept, r_value, p_value, std_err = extract_and_regress(shp,img,combo[0],combo[1])
        print "%i, %i, %.4f, %.4f, %.4f, %.4f" % (combo[0],combo[1],slope,intercept,r_value,std_err)
        
def extract_regress_plot(shp,img,xband,yband,plot=True):
    """This will do everything that extract_and_regress does but it will
    also generate a nicer plot. extract_and_regress is much simpler but
    maybe I should get rid of it?"""
    # get the pixel values
    xarr,yarr = get_arrays_for_all_geoms(shp,img,xband,yband)
    # get rid of any x,y pairs with zeros 'cause they'll screw up log and get the logs of pixel reflectance values
    x,y = prep_for_plot(xarr,yarr)
    # get the linear regression values on the logs of pixel values
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
    # make some x,y values to represent the regression line on the plot
    x_values = scipy.linspace(x.min(),x.max(),4)
    y_values = [slope * xx + intercept for xx in x_values]
    
    # Figure out how big to make the figure based on monitor size
    root = Tk()
    swidth = root.winfo_screenwidth()
    sheight = root.winfo_screenheight()
    root.destroy()
    reso = 72 # dpi
    swinch = swidth / reso
    shinch = sheight / reso - 1
    
    ## Start building the plot
    # create a figure to put the plot in
    fig = pylab.figure(figsize=(swinch,shinch),dpi=72)
    # create a subplot in the figure
    ax = fig.add_subplot(111)
    # plot the points
    ax.scatter(x,y,s=freq_list(x,y),alpha=0.05)
    #ax.hexbin(x,y,gridsize=40)
    # plot the regression line
    ax.plot(x_values,y_values,'r-')
    # add some labels
    myxlabel = r'$\ln$ of band %i' % xband
    myylabel = r'$\ln$ of band %i' % yband
    ax.set_xlabel(myxlabel) 
    ax.set_ylabel(myylabel)
    # Make the title
    title_text = "Attenuation Coefficient Ratio Plot for Bands %i and %i of %s\n Using Geometry from %s" % (xband,yband,os.path.basename(img.GetDescription()),os.path.basename(shp.name))
    ax.set_title(title_text,fontsize=24)
    # Make some text
    plot_text = "Slope " + r"$\frac{K_%i} {K_%i}$" % (yband,xband) + " = %.3f\n" % slope
    plot_text += "Depth invarient index (y intercept) = %.3f\n" % intercept
    plot_text += "r-value = %.3f\n" % r_value
    #plot_text += "p-value = %.6f\n" % p_value
    plot_text += "Derived from %i pixels from %i image areas" % ( len(x),shp.GetLayer().GetFeatureCount() )
    fig.text(0.14,.88,plot_text,va='top')
    ax.margins(0)
    #fig.savefig('test.png')
    pylab.show()
    
    return slope, intercept, r_value, p_value, std_err
    
## These following methods aren't done and may never be done. On the other hand
## I might want to finish them some day so I don't think I'll delete them.

def features_dict(lyr,fieldname='substrate',fieldname2='depth'):
    lyr.ResetReading()
    keys = [ feat.__getattr__(fieldname) for feat in lyr ]
    keys = list(np.unique(keys))
    fdict = {}
    lyr.ResetReading()
    for key in keys:
        fdict[key] = {}
    for feat in lyr:
        pass
    # Forget it for now. There's not enough return on the investment of doing this right now. maybe later
        
def multisubstrate_regression(shp,img,xband,yband):
    """Like extract_regress_plot but I want to plot multiple substrate
    types and I want to color code the points on the scatter plot."""
    lyr = shp.GetLayer()
    pass
    # Forget it for now. There's not enough return on the investment of doing this right now. maybe later
    
## End unfinished methods ############################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Perform Band Ratio Calculations for Water Column Correction of Mulitspectral Imagery.')
    parser.add_argument('-xband', nargs='?', type=int, default=2, help='The band that you want to use for the x-axis.')
    parser.add_argument('-yband', nargs='?', type=int, default=3, help='The band that you want to use for the y-axis.')
    parser.add_argument('raster', nargs='?', default = test_file, help='Path to the raster file you want to do calculations on')
    parser.add_argument('shapefile', nargs='?', default = test_regions, help='Path to the shapefile you want to do calculations on')

    
    simple_tests( parser.parse_args() )
